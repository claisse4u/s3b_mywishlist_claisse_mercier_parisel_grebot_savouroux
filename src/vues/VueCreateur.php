<?php

namespace mywishlist\vues;

use mywishlist\vues as Vue;

/**
 * Vue qui va permettre l'affichage de la pages
 * du point de vue du créateur de la liste
 */
class VueCreateur{

  private $param; //Param dnné dans le constructeur et traité dans la fonction render
  private $select; //Attribut qui permet de choisir la méthode d'affichage

  const AFF_INSC = 1; // affichage inscription
  const AFF_CO = 2; // affichage connexion
  const AFF_NO_CO = 3; // affichage lors d'aucune connexion
  const AFF_NO_ACCES = 4; // affichage interdiction
  const AFF_LIST = 5; // affichage des listes de l'utilisateur
  const AFF_LISTS_USER = 6; // affichage des listes de l'utilisateur
  const AFF_CREA_LIST  = 7; // affichage formulaire création d'une liste
  const AFF_FORM_ITEM  = 8; // affichage formulaire création d'un item
  const AFF_USER  = 9; // affichage panneaux de config utilisateur
  const AFF_MODIF_COMPTE = 10; // affichage des informations de l'utilisateur à modifier
  const AFF_MODIF_MDP = 11; // affichage de la modification du mot de passe à modifier


  public function __construct($param, $select = -1){
    $this->select = $select;
    $this->param = $param;
  }

  public function render(){
    $html = \mywishlist\vues\VueGeneral::genererHeader();
    $app = \Slim\Slim::getInstance();
    $cheminCo =  $app->urlFor('connexion');
    $cheminInsc =  $app->urlFor('inscription');

    switch($this->select){
      case -1 :
        $html .= "<p>Tu as oublier le selecteur monsieur !</p>";
          break;
      case VueCreateur::AFF_INSC : //----------------------------------------------Affichage-Inscription
//---------------------------------------------Gestion-du-cas-d'erreur
        $nom = "";
        $prenom = "";
        $date = "";
        $email = "";
        $errorMdp = "";
        $errorEmail = "";
        if(isset($this->param)){ // Gestion de l'affichage de l'erreur
          if ($this->param["error"] === "mdpDiff"){
            $errorMdp = "<p>***Mot de passe invalide !***</p>";
          }else if ($this->param["error"] === "email"){
            $errorEmail = "<p>***Email invalide !***</p>";
          }else if ($this->param["error"] === "mdpShort"){
            $errorMdp = "<p>***Mot de passe trop court !***</p>";
          }else if ($this->param["error"] === "emailExist"){
            $errorEmail = "<p>***Email existe déjà dans la base !***</p> ";
          }
          $nom = "value=\"".$this->param["nom"]."\""; //Affichage pré-rempli du formulaire en cas d'erreur
          $prenom = "value=\"".$this->param["prenom"]."\"";
          $date = "value=\"".$this->param["naissance"]."\"" ;
          $email = "value=\"".$this->param["email"]."\"";
        }
//----------------------------------------------
        $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
          $html.= <<<END
  <header>INSCRIPTION</header>
  <form id="form" method="POST" action="$cheminInsc">
    <label>Nom* : </label> <input type="text" name="nom" placeholder="Nom" $nom required>
    <label>Prénom* : </label><input type="text" name="prenom" placeholder="Prénom" $prenom required>
    <label>Date de naissance* : </label><input type="date" name="naissance" min='1899-01-01' max='2010-01-01' $date>
    $errorEmail
    <label id="email">Email* : </label><input type="email" name="email" placeholder="Email" $email required>
    $errorMdp
    <p>Doit contenir minimum 6 caractères</p>
    <label>Mot de Passe* : </label> <input type="password" name="mdp" placeholder="Mot de passe" required>
    <label>Confirmation* : </label><input type="password" name="mdp-conf" placeholder="Mot de passe" required>
    <input id="submit" type="submit" name="valider-insc" value="S'inscrire"  placeholder="Mot de passe">
  </form>
END;
      break;

      case VueCreateur::AFF_CO : //----------------------------------------------------------------------------------------------Affichage-Connexion
      $errorLogIn = "";
      $email = "";
      if(isset($this->param)){ //Gestion du cas d'erreur
        if(isset($this->param["email"]) && isset($this->param['error']) && $this->param['error'] == "auth"){
          $email = "value=\"".$this->param['email']."\"";
          $errorLogIn = "<p>*** Mauvais email ou mot de passe ***</p>";
        }
      }
      $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
      $html.= <<<END
            <header>CONNEXION</header>
            <form id="form" method="POST" action="${cheminCo}">
              <label>EMAIL : </label> <input type="email" name="email" placeholder="EMAIL" $email required>
              <label>MOT DE PASSE : </label><input type="password" name="mdp" placeholder="MOT DE PASSE" required>
              $errorLogIn
              <input id="submit" type="submit" name="connection" value="Connexion">
              <div id="no_count">
                <a>Pas de compte ? <a href="$cheminInsc" id="link">Inscrivez-vous !</a></a>
              </div>
            </form>
            </div>
END;
        break;

        case VueCreateur::AFF_LISTS_USER :  //--------------------------------------------------------------------------------Listes-utilisateurs
          $html = \mywishlist\vues\VueGeneral::genererHeader("listes");
          $class = $app->urlFor("liste-creation");
          if ($this->param != null) {
            $html .= <<<END
            <div class="aff-listes">
              <div id="add_liste">
                <a href="$class">Ajouter une liste de cadeaux</a><BR>
              </div>
              <div id="fond">
END;
            foreach ($this->param as $key => $liste) { //Affichage des listes de l'utilisateur
              $key++;
              $routeListe = $app->urlFor("aff-liste",["idList" => $liste->no]);
              $html .= <<<END
              <div class="liste">
                <a href='#' class="img">$key</a>
                <a href='$routeListe' class="nom">$liste->titre</a>
              </div>
END;
            }
          $html .= "</div>";
          } else {
            $html .= "<div class=\"aff-listes\">";
            $html .= "<h2> Oupss ! Il semble que vous n'avez pas de liste ... :( </h2> \n";
          }
          $html .= "</div>";
          break;

        case VueCreateur::AFF_CREA_LIST :  //-----------------------------------------------------------------------------------Affichage-Formulaire-creation-liste
        $titre = "";
        $desc = "";
        $timeError = "";
        $timeExpr="";
        $legend="Cr&eacute;er ta Liste !";
        $nameButton="Cr&eacute;er";
        $cheminSubmit = $app->urlFor("liste-creation");
        if(isset($this->param["error"]) && $this->param["error"] == "time" ){
          $titre = "value=\"".$this->param["titre"]."\"";
          $desc = "value=\"".$this->param["desc"]."\"";
          $timeError = "<p>*** Expiration Invalide ***</p>";
        }

        if(isset($this->param->no)){ //Si c'est appelé d'une modifcation
          $titre = "value=\"".$this->param->titre."\"";
          $desc = "value=\"".$this->param->description."\"";
          $timeExpr= "value=\"". $this->param->expiration."\"";
          $legend = "Modifier liste : ".$this->param->titre;
          $nameButton="Modifier";
          $cheminSubmit = $app->urlFor("modif-liste",["idList" => $this->param->no]);

        }

        $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
          $html.= <<<END
    <form id="form" method="POST" action="$cheminSubmit">
      <label>TITRE* : </label> <input type="text" name="titre" placeholder="TITRE" $titre required>
      <label>DESCRIPTION : </label> <input type="text" name="desc" placeholder="DESCRIPTION" $desc>
    $timeError
      <label>DATE D'EXPIRATION : </label> <input type="date" name="expir" min='2018-01-01' $timeExpr>
      <input id="submit" type="submit" name="valid-crea-list" value="$nameButton">
    </form>
END;
        break;
        case VueCreateur::AFF_FORM_ITEM :  //-----------------------------------------------------------------------------------Affichage-Formulaire-creation-item

        $numberError = "";
        $fileError = "";
        if(isset($this->param["error"]) ){ //Erreur du formulaire
          if($this->param["error"] == "number" ){
            $numberError = "<p>*** Format prix Invalide ***</p>";
          }
          if($this->param["error"] == "upload" ){
            $fileError = "<p>*** Une erreur est survenue ***</p>";
          }
          if($this->param["error"] == "extension" ){
            $fileError = "<p>*** Format fichier Invalide ***</p>";
          }
          if($this->param["error"] == "file_size" ){
            $fileError = "<p>*** Taille du fichier trop gros ***</p>";
          }
        }
        $cheminCreaItem = $app->urlFor("ajout-item",['idList' => $this->param['idListe']]);
        $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
          $html.= <<<END
    <form id="form" method="POST" action="$cheminCreaItem"  enctype="multipart/form-data">
      <label>Nom* : </label> <input type="text" name="nom" placeholder="Nom de l'item ..." required>
      <label>Description : </label> <input type="text" name="desc" placeholder="Votre description ..." >
      $numberError
      <label>Prix* : </label> <input type="number" name="prix" required>
      $fileError
      <label>Image : </label>
      <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
      <input id="ajoutPhoto" type="file" name="image">
      <label>URL : </label> <input type="url" name="url">
      <input id="submit" type="submit" name="valid-crea-item" value="Ajouter">
    </form>
END;
        break;


        case VueCreateur::AFF_LIST :  //--------------------------------------------------------------------------------------Affichage-Liste-avec-ses-items

        $titreListe = $this->param['liste']->titre;

        if($this->param['liste']->description != null) //Affichage description
          $description = $this->param['liste']->description;
        else {
          $description = "";
        }
        if($this->param['liste']->expiration != null) //Affichage de la date d'expiration
          $dateExpiration = $this->param['liste']->expiration ;
        else {
          $dateExpiration = "";
        }
        $cheminMakeToken = $app->urlFor("make-token",["idList" => $this->param['liste']->no]);
        $token = "<a href=\"$cheminMakeToken\" class=\"block block-act\">Partager la liste</a>";

        if($this->param["liste"]->token != null){ // Affichage du token si initialisé
          $cheminToken = "http://".$_SERVER['HTTP_HOST'].$app->request->getRootUri().'/'.$this->param["liste"]->token;
          $token = "<a href=\"$cheminMakeToken\" class=\"block block-act\">Créer un nouveau lien</a>
        <label>Lien Partageable : </label> <input type=\"text\" value=\"$cheminToken\">";
        }


        $cheminAddItem = $app->urlFor("ajout-item", [ "idList" => $this->param["liste"]->no]);
        $cheminModifList = $app->urlFor("modif-liste", [ "idList" => $this->param["liste"]->no]);
        $cheminSupprList = $app->urlFor("del-liste", [ "idList" => $this->param["liste"]->no]);
        $html = \mywishlist\vues\VueGeneral::genererHeader("item");
          $html.= <<<END
    <div class="aff-items">
      <div class="box-param">
        <a href="$cheminAddItem" class="block block-act">Ajouter</a>
        <a href="$cheminModifList" class="block block-act">Modifier</a>
        <a href="#idSuppList" class="block block-act">Supprimer</a>
        $token
        <div id="idSuppList" class="modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <header class="container">
              <a href="#" class="closebtn">×</a>
                <h4>Suppression d&eacute;finitive de la liste</h4>
              </header>
              <div class="container">
                <p>Supprimer la liste ? </p>
                <form class="reservation" method="GET" action="$cheminSupprList">
                    <button class="suppr" type="submit">Supprimer</button>
                    <a href="#">Annuler</a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="box box-trans">
        <h1 class="block block-wr" id="titre">$titreListe</h1>
        <h4 class="block block-wr" id="date">$dateExpiration</h4>
        <h6 class="block block-wr" id="description">$description</h6>
      </div>
      <div class="box box-item">
END;
        if (count($this->param['itemList'])>0) { //Affichage des items
          foreach ($this->param['itemList'] as $value) {
            $message="";
            $reserver =<<<END
            <p class="etat" style="color : green">Libre</p>
END;
            foreach ($this->param['reserv'] as $reservation) {
              if($reservation->id_item == $value->id){
                $reserver =<<<END
                <p class="etat" style="color : red">R&eacute;serv&eacute;</p>
END;
              if($reservation->message != null || $reservation->message != ''){
                $message="Message: ".$reservation->message;
              }
              }
            }
            if($value->img != ''){
              $cheminImg = $app->request->getRootUri()."/img/".$value->img;
            }else{
              $cheminImg = $app->request->getRootUri()."/img/interrogation.png";
            }
            $altImg = $value->nom;
            $cheminDelete = $app->urlFor("remove-item",["idList" => $value->liste_id, "idItem"=> $value->id]);
            $html.= <<<END
              <div class="item">
                  <img src="$cheminImg" alt="$altImg">
                  <div class="description">
                    <h4>$value->nom</h4>
                    <p id="descItem">$value->descr</p>
                    <p id="messageItem">$message</p>
                  </div>
                  $reserver
                  <a href="#id$value->id" class="supprimer">X</a>
              </div>
              <div id="id$value->id" class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <header class="container">
                    <a href="#" class="closebtn">×</a>
                      <h4>Suppression Item</h4>
                    </header>
                    <div class="container">
                      <p>Supprimer l'item $value->nom ? </p><br>
                      <form class="reservation" method="GET" action="$cheminDelete">
                          <button class="suppr" type="submit" name="valid-reserv" value="valid_reserv">Supprimer</button>
                          <a href="#">Annuler</a>
                      </form>

                    </div>
                  </div>
                </div>
              </div>
END;
          }
        } else {
          $html.= "<p>Aucun article...</p>";
        }

        $html.= <<<END
        </div>
        </div>
END;
        break;

        case VueCreateur::AFF_NO_CO :  //-----------------------------------------------------------------------Erreur-Non-conecté
          $html = \mywishlist\vues\VueGeneral::genererHeader("erreur");
          $html .= "<h1> Oupss ! Il semble que vous n'&ecirc;tes pas connect&eacute ... :( </h1> \n";
          $html .= <<<END
          <p>Inscrivez-vous sur <a href="${cheminInsc}">ce lien</a></br>Ou si vous &ecirc;tes d&eacutej&agrave; inscrit c'est sur <a href="${cheminCo}">celui-ci</a> </p>
END;
        break;
        case VueCreateur::AFF_NO_ACCES :  //------------------------------------------------------------------------interdiction-Acces
          $accueil = $app->urlFor('accueil');
          $html = \mywishlist\vues\VueGeneral::genererHeader("erreur403");
          $html .= <<<END
          <div class="message">Erreur 403 : Vous n&#39;avez pas le droit d'acc&egrave;s.</div>
            <div class="message2">Retour &agrave; la <a href="$accueil">page d&#39;accueil</a></div>
            <div class="container">
              <div class="neon">403</div>
              <div class="door-frame">
                <div class="door">
                  <div class="rectangle">
                </div>
                  <div class="handle">
                    </div>
                  <div class="window">
                    <div class="eye">
                    </div>
                    <div class="eye eye2">
                    </div>
                    <div class="leaf">
                    </div>
                  </div>
                </div>
              </div>
            </div>
END;
        break;

        case VueCreateur::AFF_USER  : //------------------------------------------------------------------------------Configuration-Utilisateur
        $cheminDelete = $app->urlFor('supprimer-compte');
        $cheminCompteInfo = $app->urlFor('modifier-infocompte');
        $cheminModifMdp = $app->urlFor('modifier-mdp');
        $html = \mywishlist\vues\VueGeneral::genererHeader("item");
        $html .= <<<END
        <div id="bouton">
          <a href="$cheminCompteInfo">Modifier son compte</a>
          <a href="$cheminModifMdp">Changer de mot de passe</a>
          <a href="#sup-compte">Supprimer son compte</a>
        </div>
        <div id="sup-compte" class="modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <header class="container">
              <a href="#" class="closebtn">×</a>
                <h4>Supprimer son compte</h4>
              </header>
              <div class="container">
                <p> Voulez-vous réellement supprimer votre compte ? </p><br>
                <form class="reservation" method="GET" action="$cheminDelete">
                    <button class="suppr" type="submit" name="valid-reserv" value="valid_reserv">Supprimer</button>
                    <a href="#">Annuler</a>
                </form>
              </div>
            </div>
          </div>
        </div>
END;

        break;

        case VueCreateur::AFF_MODIF_COMPTE :
        $cheminCompte = $app->urlFor('modifier-infocompte');
        $nom = "value=\"".$_SESSION['profile']['nom']."\"";
        $prenom = "value=\"".$_SESSION['profile']['prenom']."\"";
        $date = "value=\"".$_SESSION['profile']['datenaiss']."\"";
        $email = "value=\"".$_SESSION['profile']['email']."\"";

        $emailError = "";
        $emailExist = "";
        if(isset($this->param))
          if ($this->param === "emailExist")
            $emailExist = "<p>***Email existe déjà dans la base !***</p>";
            else {
              if ($this->param === "emailError")
                $emailError = "<p>***Email invalide !***</p>";
            }

        $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
        $html .= <<<END
        <header>MODIFIER SON COMPTE</header>
        <form id="form" method="POST" action="$cheminCompte">
          <label>Nom* : </label> <input type="text" name="nom" placeholder="Nom" $nom required>
          <label>Prénom* : </label><input type="text" name="prenom" placeholder="Prénom" $prenom required>
          <label>Date de naissance* : </label><input type="date" name="naissance" min='1899-01-01' max='2010-01-01' $date>
          $emailError
          $emailExist
          <label>Adresse email* : </label><input type="text" name="email" $email required>
          <input id="submit" type="submit" name="valider-insc" value="Modifier">
        </form>
END;
        break;

        case VueCreateur::AFF_MODIF_MDP :
        $cheminMdp = $app->urlFor('modifier-mdp');
        $errorLogIn = "";
        if(isset($this->param) && $this->param == 'error'){
          $errorLogIn = "<p>*** Mot de passe invalide ***</p>";
        }
        $html = \mywishlist\vues\VueGeneral::genererHeader("formulaire");
        $html .= <<<END
        <header>CHANGER DE MOT DE PASSE</header>
        <form id="form" method="POST" action="${cheminMdp}">
        $errorLogIn
                <label>ANCIEN MOT DE PASSE* : </label> <input type="password" name="old-pass" placeholder="ANCIEN MOT DE PASSE"  required>
                <label>NOUVEAU MOT DE PASSE* : </label><input type="password" name="new-pass" placeholder="MOT DE PASSE" required>
                <label>CONFIRMATION* : </label><input type="password" name="conf-pass" placeholder="CONFIRMATION" required>
            <input id="submit" type="submit" name="connection" value="Changer de mot de passe">
        </form>
        </div>
END;
        break;
    }
    $html .= \mywishlist\vues\VueGeneral::genererFooter();
  echo $html;
  }
}
