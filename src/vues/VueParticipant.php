<?php

  namespace mywishlist\vues;

  /**
   * Vue qui va permettre l'affichage de la pages
   * du point de vue du participant de la liste
   */
  class VueParticipant{

    private $param; //Param donné dans le constructeur et traité dans la fonction render
    private $select; //Attribut qui permet de choisir la méthode d'affichage

    const AFF_LISTE = 1; // affichage inscription
    const AFF_LISTE_NOT_FOUND = 2; // affichage inscription

    public function __construct($param, $select = -1){
      $this->select = $select;
      $this->param = $param;
    }

    public function render(){
      $html = \mywishlist\vues\VueGeneral::genererHeader("item");
      $app = \Slim\Slim::getInstance();
      $cheminCo =  $app->urlFor('connexion');
      $cheminInsc =  $app->urlFor('inscription');

      switch($this->select){
        case -1 :
          $html .= "<p>Tu as oublier le selecteur monsieur !</p>";
            break;

        case VueParticipant::AFF_LISTE :

                $titreListe = $this->param['liste']->titre;
                $nomPerson = "";
                if(isset($_COOKIE['NomParticip'])){  //Permet de rentrer le nom du participant dans le formulaire si il a déjà reservé un item
                  $nomPerson = "value=\"".$_COOKIE['NomParticip']."\"";
                }
                if($this->param['liste']->description != null) //Affiche la description de l'item
                  $description = "<p> ".$this->param['liste']->description ." </p>";
                else {
                  $description = "";
                }
                if($this->param['liste']->expiration != null) // Affichage de la date d'expiration
                  $dateExpiration = $this->param['liste']->expiration;
                else {
                  $dateExpiration = "";
                }
                  $html.= <<<END
                  <div class="aff-items">
                    <div class="box box-trans">
                      <h1 class="block block-wr" id="titre">$titreListe</h1>
                      <h4 class="block block-wr" id="date">$dateExpiration</h4>
                      <h6 class="block block-wr" id="description">$description</h6>
                    </div>
                    <div class="box box-item">
END;

                $estCreateur = false;
                if(isset($_SESSION['profile'])){ //Bloque la reservation si la condition l'a reconnu comme créateur
                  foreach ($_SESSION['profile']['liste'] as $value) {
                    if($value == $this->param['liste']->no){
                      $estCreateur = true;
                      break;
                    }
                  }
                }
                if (count($this->param['itemList'])>0) {
                  foreach ($this->param['itemList'] as $value) { //Chargement de la vue de chaque item de la liste
                    $reserver =<<<END
                    <p class="etat" style="color : green">Libre</p>
                    <a class="etat reserve" href="#id$value->id">R&eacute;server</a>
END;

                    if(isset($_COOKIE[$value->liste_id]) || $estCreateur){
                      $reserver =<<<END
                      <p class="etat" style="color : green">Libre</p>
                      <p class="etat resCrea">R&eacute;server</p>
END;
                    }
                    foreach ($this->param['reserv'] as $reservation) {
                      if($reservation->id_item == $value->id){
                        $reserver = <<<END
                        <p class="etat" style="color : red">R&eacute;serv&eacute;</a>
END;
                      if(!isset($_COOKIE[$value->liste_id]) && !isset($_SESSION['profile'])){
                        $reserver.=<<<END
                        <p class="etat whoReserve">$reservation->nom_reserv</p>
END;
                      } else {
                        $reserver.=<<<END
                        <p class="etat whoReserve">Inconnu</p>
END;
                      }
                  }
                  }
                  if($value->img != ''){ //Affichage de l'image
                    $cheminImg = $app->request->getRootUri()."/img/".$value->img;
                  }else{
                    $cheminImg = $app->request->getRootUri()."/img/interrogation.png";
                  }
                    $altImg = $value->nom;
                    $cheminReserver = $app->urlFor("reserv-item", ["token" => $this->param["liste"]->token, 'idItem' => $value->id]);
                    if( $value->descr != '' || $value->descr != null  ){ // Affichage de la description
                      $desc = "<p id=\"descItem\">".$value->descr."</p>";
                    }else{
                      $desc = "";
                    }
                    $html.= <<<END
                      <div class="item">
                          <img src="$cheminImg" alt="$altImg">
                          <div class="description">
                            <h4>$value->nom</h4>
                            $desc
                          </div>
                          $reserver
                          <p class="etat">$value->tarif&euro;</p>
                      </div>
                      <div id="id$value->id" class="modal">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <header class="container">
                            <a href="#" class="closebtn">×</a>
                              <h4>R&eacute;server</h4>
                            </header>
                            <div class="container">
                              <p>R&eacute;server l'item $value->nom ? </p><br>
                              <form class="reservation" method="POST" action="$cheminReserver">
                                  <label>Votre nom : </label><input type="text" name="nom" placeholder="Votre nom ..." $nomPerson required>
                                  <label>Un message pour le créateur de la liste ? : </label><input type="text" name="message" placeholder="Votre message ...">
                                  <button type="submit" name="valid-reserv" value="valid_reserv">R&eacute;server</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
END;


                  }
                } else {
                  $html.= "<p>Aucun article...</p>";
                }
                $html.= <<<END
                    </div>
                  </div>
END;
          break;
          case VueParticipant::AFF_LISTE_NOT_FOUND :
            $html = \mywishlist\vues\VueGeneral::genererHeader("erreur404");
            $html .= <<<END
            <h1>Oupss on a pas trouv&eacute; votre page</h1>
            <p class="zoom-area">V&eacute;rifier bien que le lien copi&eacute; soit le bon<br>
              Sinon vous pouvez vous inscrire sur <a href="${cheminInsc}">ce lien</a></br>Ou si vous &ecirc;tes d&eacutej&agrave; inscrit c'est sur <a href="${cheminCo}">celui-ci</a>
            </p>
            <section class="error-container">
              <span>4</span>
              <span><span class="screen-reader-text">0</span></span>
              <span>4</span>
            </section>
END;
          break;
      }

      $html .= \mywishlist\vues\VueGeneral::genererFooter();
      echo $html;
    }
  }
