<?php

namespace mywishlist\vues;

/**
 * Vue qui va permettre l'affichage de la page d'accueil commune
 * à tout les utilisateurs
 */
class VueAccueil{

  private $select; //Attribut qui permet de choisir la méthode d'affichage

  const AFF_ABOUT = 1;
  const AFF_CONTACT = 2;
  const AFF_HELP = 3;


  public function __construct($select = -1){
    $this->select = $select;
  }

/**
 * génère une page HTML selon le selecteur en attribut 
 */
public function render(){
  $app = \Slim\Slim::getInstance();
  $cheminCo =  $app->urlFor('connexion');
  $cheminInsc =  $app->urlFor('inscription');
  switch($this->select){
    case -1 :
      $html = \mywishlist\vues\VueGeneral::genererHeader("demarrage");
      $html .=
      <<<END
  <h1>Bienvenue sur le site <strong>myWishList</strong></h1>
  <h3> MyWishList est un site qui permet de partager vos idées de cadeaux lors d'un événement</h3>
  <p> Groupe : Claisse Julien | Grebot Lucas | Savouroux Florian | Mercier Clément | Parisel Guillaume </p>
  <a id="link" href="https://bitbucket.org/claisse4u/s3b_mywishlist_claisse_mercier_parisel_grebot_savouroux/">Lien d&eacute;p&ocirc;t BitBucket</a>
END;
    break;

    case VueAccueil::AFF_ABOUT:
    $html = \mywishlist\vues\VueGeneral::genererHeader("about");
    $html .= <<<END
    <h1>A propos de nous</h1>
    <h3>Qui sommes-nous ?</h3>
    <p>Nous sommes 5 étudiants en deuxième année de DUT Informatique à Nancy.<BR>
      Groupe B : Claisse Julien | Grebot Lucas | Savouroux Florian | Mercier Clément | Parisel Guillaume</p>
    <h3>Pourquoi ?</h3>
    <p>Ce site internet est un des projets que nous avons à faire au cours de
      notre année d'étude. <BR>Il nous a été demandé de faire un site permettant la
      création de listes de cadeaux pouvant être partagées.</p>
END;
    break;

    case VueAccueil::AFF_HELP:
    $html = \mywishlist\vues\VueGeneral::genererHeader("help");
    $html .= <<<END
    <h1>Besoin d'aide ?</h1>
    <h3>Comment ca marche ?</h3>
    <p>myWishList est un outil vous permettant de créer une ou plusieurs listes de cadeaux que vous souhaitez partagées.<BR>
      Vous pouvez également consulter les listes qui vous ont été partagé.<BR>
      Avant toute chose, il faut vous créer un compte ou vous connectez si cela est déjà fait.<BR>
      Pour ce faire, il suffit de cliquer sur les onglets en haut à droite de la page.<BR></p>
END;
    break;

    }


    $html .= \mywishlist\vues\VueGeneral::genererFooter();
    echo $html;
  }
}
