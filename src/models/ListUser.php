<?php

  namespace mywishlist\models;
  /**
   * Classe modélisant la table liste
   */
  class ListUser extends  \Illuminate\Database\Eloquent\Model {

      //Parametre de la table
      protected $table = 'listuser';   //Nom de la table
      protected $primaryKey = 'uid'; //Clé Primaire de la table
      public $timestamps = false;   //Non ajout des attributs de la date d'insertion et de modification


  }
