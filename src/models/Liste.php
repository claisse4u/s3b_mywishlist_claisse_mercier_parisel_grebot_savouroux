<?php

  namespace mywishlist\models;
  /**
   * Classe modélisant la table liste
   */
  class Liste extends  \Illuminate\Database\Eloquent\Model {

      //Parametre de la table
      protected $table = 'liste';   //Nom de la table
      protected $primaryKey = 'no'; //Clé Primaire de la table
      public $timestamps = false;   //Non ajout des attributs de la date d'insertion et de modification

      /**
       *  Méthode permettant d'obtenir les item d'une liste donnée
       *  grâce à une jointure
       */
      public function items(){
        //Param : namespace de la classe , 'clé étrangere'
        return $this->hasMany('\mywishlist\models\Item', 'liste_id');
      }
  }
