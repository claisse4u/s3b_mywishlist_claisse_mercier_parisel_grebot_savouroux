<?php

  namespace mywishlist\models;
  /**
   * Classe modélisant la table item
   */
  class Item  extends  \Illuminate\Database\Eloquent\Model {

      //Parametre de la table
      protected $table = 'item';  //Nom de la table
      protected $primaryKey = 'id'; //Clé Primaire de la table
      public $timestamps = false;   //Non ajout des attributs de la date d'insertion et de modification

      /**
       *  Méthode permettant d'obtenir la liste de l'item
       *  grâce à une jointure
       */
      public function list(){
        //Param : namespace de la classe , 'clé étrangere'
        return $this->belongsTo('\mywishlist\models\liste', 'liste_id');
      }
  }
