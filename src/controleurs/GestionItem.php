<?php

namespace mywishlist\controleurs;

use mywishlist\models as Model ;
use mywishlist\vues\VueCreateur as VueCreateur;

/**
 * Controleur qui va gerer l'organisation
 * des items dans la liste
 */
class GestionItem{

  public function afficherFormulaireItem($idListe){
    $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
        $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idListe);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }
      $param['idListe'] = $idListe;
    $vue = new VueCreateur($param, VueCreateur::AFF_FORM_ITEM);
    $vue->render();
  }

  /**
   * Fonction supprimant un item d'une liste selon $idList et $idItem
   * La reservation et l'image associées seront aussi supprimées
   */
  public function supprimerItem($idList,$idItem){
    $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
        $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idList);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

    $ItemDel = Model\Item::where("id","=",$idItem,"and","liste_id","=",$idList)->first();
    if($ItemDel != null){
      $ItemResDel = Model\Reservation::where("id_item","=",$idItem)->first();
      if($ItemResDel != null){
        $ItemResDel->delete();
      }
      if($ItemDel->img != null){
        $nomFichier = "img/".$ItemDel->img;
        unlink($nomFichier);
      }
      $ItemDel->delete();
    }

    $app->redirect( $app->urlFor("aff-liste",["idList" => $idList]));
  }

  /**
   *  Ajoute un item à une liste donnée
   */
  public function ajouterItem($value,$idList, $img){

    $param['idListe'] = $idList;
    $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
        $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idList);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      if($img["image"]["name"] != ""){
        $image = $this->gestionImage($img,$idList);
      } else {
        $image = null;
      }


        if(isset($value['nom']) && isset($value['desc']) && isset($value['prix']) && isset($value['url']) && $image != "error"){
          try{
            $valueFiltred = $this->filtrerValeurItem($value);
            $newItem = new Model\Item();
            $newItem->nom = $valueFiltred['nom'];
            $newItem->descr = $valueFiltred['desc'];
            $newItem->url = $valueFiltred['url'];
            $newItem->tarif = $valueFiltred['prix'];
            $newItem->liste_id = $idList;
            $newItem->img = $image;

            $newItem->save();

            $app->redirect( $app->urlFor("aff-liste",["idList" => $idList]));

          }catch(\mywishlist\models\InvalideNumberException $ine){
            $param['error'] = "number";
            $vue = new VueCreateur($param, VueCreateur::AFF_FORM_ITEM);
            $vue->render();
          }
        }

  }

  public function filtrerValeurItem($value){

      $tab['nom'] = filter_var($value['nom'] , FILTER_SANITIZE_STRING);
      $tab['desc'] = filter_var($value['desc'] , FILTER_SANITIZE_STRING);
      $tab['url'] = filter_var($value['url'] , FILTER_SANITIZE_URL) ;
      $tab['prix'] = intval(filter_var($value['prix'] , FILTER_SANITIZE_NUMBER_INT)) ;
      if( !filter_var( $value['prix'], FILTER_VALIDATE_INT))
        throw new \mywishlist\models\InvalideNumberException("Format invalide");

      return $tab;
  }

  /**
   * Fonction permettant de teter la validité de l'image et la copier sur le serveur avec un nom unique
   */
  public function gestionImage($image, $idListe){
    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );

    $erreur = "";

    try {
      if ($image['image']['error'] > 0)
        throw new \mywishlist\models\FileException("upload");

        if ($image['image']['size'] > 1048576)
        throw new \mywishlist\models\FileException("file_size");


        $extension_upload = strtolower(  substr(  strrchr($image['image']['name'], '.')  ,1)  );

        if (! in_array($extension_upload,$extensions_valides) )
          throw new \mywishlist\models\FileException("extension");

        $id_membre = md5(uniqid(rand(), true));

        $nom = "img/{$id_membre}.{$extension_upload}";
        $resultat = move_uploaded_file($image['image']['tmp_name'],$nom);

        $retour =  $id_membre.".".$extension_upload ;
      } catch (\mywishlist\models\FileException $fe) { // Génération de l'erreur si l'image est invalide
        $param['error'] = $fe->getMessage();
        $param['idListe'] = $idListe;
        $vue = new VueCreateur($param, VueCreateur::AFF_FORM_ITEM);
        $vue->render();
        $retour = "error";
      }

      return $retour;
  }
}
