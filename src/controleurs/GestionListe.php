<?php

namespace mywishlist\controleurs;

use mywishlist\models as Model ;
use mywishlist\vues as Vue;


/**
 * Controleur qui affiche toutes les listes présente dans la base de donnée
 */
class GestionListe{

  /**
   * Réserve un item donné
   */
  public function reserverItem($token, $id, $param){
    $tab["nom"] = filter_var($param['nom'] , FILTER_SANITIZE_STRING);
    $tab["message"] = filter_var($param['message'] , FILTER_SANITIZE_STRING);

    $reserv = new Model\Reservation();

    $item = Model\Item::where("id","=",$id)->first();
    $r = Model\Reservation::where("id_item","=",$id)->first();

    if($item != null && $r == null ){
      $reserv->id_item = $id;
      $reserv->nom_reserv = $tab["nom"];
      $reserv->message = $tab["message"];
      $reserv->save();
    }
    //Cookie pour le nom de l'utilisateur
    if(!isset($_COOKIE['NomParticip'])){
      setcookie("NomParticip",$tab['nom']);
    }

    $app = \Slim\Slim::getInstance();
    $app->redirect( $app->urlFor("aff-token", [ "token" => $token ]  )) ;
  }

    /**
     * Affichage d'une liste participant
     * selon le token qui lui a été donnée
     * si le token est inexistant affiche une page d'erreur
     */
    public function afficherListeParticipant($token){

      $listeInfo = Model\Liste::where("token","=",$token)->first();

      if($listeInfo != null){
        $ItemList = Model\Item::where("liste_id","=",$listeInfo->no)->get();
        $reservList = Model\Reservation::get();
        $param['liste'] = $listeInfo;
        $param['itemList'] = $ItemList;
        $param["reserv"]= $reservList;
        $vue = new Vue\VueParticipant($param,Vue\VueParticipant::AFF_LISTE);
        $vue->render();
      } else {
        $app = \Slim\Slim::getInstance();
        $app->redirect( $app->urlFor("list-not-found")  ) ;
      }
    }


    public function afficherlisteNotFound(){
      $vue = new Vue\VueParticipant(null,Vue\VueParticipant::AFF_LISTE_NOT_FOUND);
      $vue->render();
    }

    /**
     *  Affiche toutes les listes de l'utilisateur connecté
     */
    public function afficherAllListes(){
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app = \Slim\Slim::getInstance();
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      $vue = new Vue\VueCreateur(Model\Liste::where("user_id","=",$_SESSION['profile']["uid"])->get() , Vue\VueCreateur::AFF_LISTS_USER);
      $vue->render();
    }

    /**
     * Affiche le formulaire de création d'une liste
     */
    public function afficherCreationListe(){
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app = \Slim\Slim::getInstance();
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }
      $vue = new Vue\VueCreateur(null, Vue\VueCreateur::AFF_CREA_LIST);
      $vue->render();
    }

  /**
   * Affiche une liste dont son id a été passé en parametre
   */
    public function afficherListe($idListe){
      $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idListe);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      $listeInfo = Model\Liste::where("no","=",$idListe)->first();
      $ItemList = Model\Item::where("liste_id","=",$idListe)->get();
      $reservList = Model\Reservation::get();
      $param['liste'] = $listeInfo;
      $param['itemList'] = $ItemList;
      $param["reserv"]= $reservList;
      $vue = new Vue\VueCreateur($param, Vue\VueCreateur::AFF_LIST);
      $vue->render();
    }

    /**
     * Supprime une liste utilisateur s'occupe de supprimer aussi les items les images et leurs reservations associé
     */
    public function supprimerListe($idList){
      $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idList);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      $items = Model\Item::where("liste_id","=",$idList)->get();
      foreach ($items as $item) {
        $ItemDel = Model\Item::where("id","=",$item->id,"and","liste_id","=",$idList)->first();
        $reservDel = Model\Reservation::where("id_item","=",$item->id)->first();
        if($reservDel != null){
          $reservDel->delete();
        }
        if($ItemDel != null ){
          if($ItemDel->img != null && $ItemDel->img != '' ){
            $nomFichier = "img/".$ItemDel->img;
            unlink($nomFichier);
          }
          $ItemDel->delete();
        }
      }

      $liste = Model\Liste::where("no","=",$idList);
      $liste->delete();

      //Redirige vers la liste des listes de l'utilisateur
      $app->redirect( $app->urlFor("liste"));
    }

    /**
     * Définit un token permetttant de générer un lien partageable de la liste en question
     */
    public function setToken($idListe){
      $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }
      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idListe);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      $token = md5(uniqid(rand(), true));
      $liste = Model\Liste::where("no","=",$idListe)->first();

      if($liste != null){
        $liste->token = $token;
        $liste->save();
      }

      if(!isset($_COOKIE[$idListe])){
        setcookie($idListe,$idListe,0,$app->request->getRootUri());
      }
      $app->redirect( $app->urlFor("aff-liste",["idList" => $idListe]));

    }
    /**
     * Créer une liste a partir des données du formulaire (Titre , description , date d'expiration)
     */
    public function creerListe($param){
      if( $param != null ){

        $paramFiltr = $this->filtrerListeCreee($param); //Filtrage des données
            $error = false;
            $newList = new Model\Liste();
            $newList->user_id = $_SESSION['profile']['uid'];
            $newList->titre = $paramFiltr["titre"];
            if(isset($paramFiltr["desc"]) && $paramFiltr["desc"] != "" ) // La description peut être null
              $newList->description = $paramFiltr["desc"];

            if(isset($paramFiltr["expir"]) && $paramFiltr["expir"] != ""){ // La date d'expiration peut être null
              if(strtotime( $paramFiltr["expir"]) > strtotime(date("Y-m-d"))){ // Test de la validité de la date d'expiration
                  $newList->expiration = $paramFiltr["expir"];
              } else { //Si non valide renvoie le formulaire avec les données pré-remplis
                $error = true;
                $paramFiltr["error"] = "time";
                $vue = new Vue\VueCreateur($paramFiltr, Vue\VueCreateur::AFF_CREA_LIST);
                $vue->render();
              }
            }

            if(!$error){ //Sauvegarde et redirection vers la nouvelle liste
              $newList->save();
              $_SESSION["profile"]["liste"][] = $newList->no; //Mis a jour des listes auxquelles l'utilisateurs à accès
              $app = \Slim\Slim::getInstance();
              $app->redirect( $app->urlFor("aff-liste", [ "idList" => $newList->no ] )  ) ;
            }
      }
    }

    /**
     * Filtre les données du formulaire d'une nouvelle liste
     */
    public function filtrerListeCreee($param){
      if(isset($param['titre']))
        $tab['titre'] = filter_var($param['titre'] , FILTER_SANITIZE_STRING);

      if(isset($param["desc"]) && $param["desc"] != "" )
        $tab['desc'] = filter_var($param['desc'] , FILTER_SANITIZE_STRING);

      if(isset($param["expir"]) && $param["expir"] != "" )
        $tab['expir'] = filter_var($param['expir'] , FILTER_SANITIZE_STRING) ;
      return $tab;
    }

    /**
     * Affiche le formulaire pré-rempli de la modification d'une liste
     */
    public function afficherModifListe($idListe){
      $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idListe);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      $listeInfo = Model\Liste::where("no","=",$idListe)->first();

      $vue = new Vue\VueCreateur($listeInfo, Vue\VueCreateur::AFF_CREA_LIST);
      $vue->render();
    }

    public function modifierListe($idList,$param){
      $app = \Slim\Slim::getInstance();
      //Redirection si l'utilisateur n'est pas connecté
      if(!isset($_SESSION["profile"])){
          $app->redirect( $app->urlFor("no-connection")  ) ;
      }

      //Redirection si l'utilisateur n'a  pas accès a la liste correspondante
      try{
        Model\Authentication::checkAccessRights($idList);
      }catch (\mywishlist\models\AuthException $ae){
        $app->redirect( $app->urlFor("no-access"));
      }

      $paramFiltr = $this->filtrerListeCreee($param); //Filtrage des données

      $error = false;
      $newList = Model\Liste::where("no","=",$idList)->first();
      $newList->titre = $paramFiltr["titre"];
      if(isset($paramFiltr["desc"]) && $paramFiltr["desc"] != "" ) // La description peut être null
        $newList->description = $paramFiltr["desc"];

      if(isset($paramFiltr["expir"]) && $paramFiltr["expir"] != ""){ // La date d'expiration peut être null
        if(strtotime( $paramFiltr["expir"]) > strtotime(date("Y-m-d"))){ // Test de la validité de la date d'expiration
            $newList->expiration = $paramFiltr["expir"];
        } else { //Si non valide remvoie le formulaire avec les données pré-remplis
          $error = true;
          $vue = new Vue\VueCreateur($newList, Vue\VueCreateur::AFF_CREA_LIST);
          $vue->render();
        }
      }

      if(!$error){ //Sauvegarde et redirection vers la nouvelle liste
        $newList->save();
        $app->redirect( $app->urlFor("aff-liste", [ "idList" => $newList->no ] )  ) ;
      }
    }

}
