<?php

namespace mywishlist\controleurs;

/**
 * Controleur qui gère les fonctionnalités présente sur
 * la page d'accueil commun à chaque personne
 */
class GestionAccueil {

  public function afficheAccueil(){
    $vue = new \mywishlist\vues\VueAccueil();
    $vue -> render();
  }

  public function afficheAbout(){
    $vue = new \mywishlist\vues\VueAccueil(\mywishlist\vues\VueAccueil::AFF_ABOUT);
    $vue->render();
  }

  public function afficheContact(){
    $vue = new \mywishlist\vues\VueAccueil(\mywishlist\vues\VueAccueil::AFF_CONTACT);
    $vue->render();
  }

  public function afficheHelp(){
    $vue = new \mywishlist\vues\VueAccueil(\mywishlist\vues\VueAccueil::AFF_HELP);
    $vue->render();
  }
}