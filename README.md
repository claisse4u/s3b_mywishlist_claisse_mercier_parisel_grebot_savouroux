# README #

Ce README a pour but de vous guidez pour l'installation de mywishlist sur votre server

### Outils Requis ###

* Apache avec le module rewrite actif
* PHP Version >= 7.0
* Base de donnée : MySQL
* PHPMyAdmin

### Installation ###

- Cloner le dépot git à la racine de votre serveur
- Configurer la base de donnée grâce à l'importation du fichier extension_table.sql se trouvant dans le dossier /doc
- Créer un dossier conf dans le /src et créer le ficher conf.ini avec toutes la configuration nécessaire à la communication à la base de donnée

***************************************
* Exemple de fichier /src/conf/conf.ini
*
* driver    = "mysql"
* host      = "localhost"
* database  = "__database__"
* username  = "__user__"
* password  = "__Password__"
* charset   = "utf8"
* collation = "utf8_unicode_ci"
* prefix    = ""
***************************************

- Créer un fichier .htaccess selon le model suivant dans la racine du projet

***************************************
* Model du fichier /.htaccess
*
* RewriteEngine On
*
* # Pour interdire l'accès aux répertoires contenant du code
* RewriteRule ^sql(/.*|)$ - [NC,F]
* RewriteRule ^src(/.*|)$ - [NC,F]
* RewriteRule ^vendor(/.*|)$ - [NC,F]
*
* # réécriture pour slim
*
* RewriteCond %{REQUEST_FILENAME} !-d
* RewriteCond %{REQUEST_FILENAME} !-f
* RewriteRule ^ index.php [QSA,L]
****************************************

Lancer la commande suivante à la racine du projet : 
$ composer install

*** Et ça y est ! Le site est fonctionnel ! ***

** Un compte par défaut existe dans la base **

Pour vous y connecter voici les identifiants 
	- email : test@test.com
	- Mot de passe : azerty

Le nom TestSession doit apparaître en haut à droite de votre navigateur.

### Le site reste disponible à l'adresse suivante ###

https://webetu.iutnc.univ-lorraine.fr/www/claisse4u/mywishlist/

### Membres du projet classe S3B ###

CLAISSE Julien
GREBOT Lucas
MERCIER Clément
PARISEL Guillaume
SAVOUROUX Florian
