<?php
// Autoload
require 'vendor/autoload.php';

//------------------ALIAS-----------------------//
use \Slim\Slim;
use Illuminate\Database\Capsule\Manager as DB;
use \mywishlist\controleurs\GestionListe;
use \mywishlist\controleurs\GestionAccueil;
use \mywishlist\controleurs\GestionCompte;
use \mywishlist\controleurs\GestionItem;


$filename = 'src/conf/conf.ini';

// Connection à la base de donnée
$db = new DB();
$db -> addConnection(parse_ini_file($filename));
$db -> setAsGlobal();
$db -> bootEloquent();

$app = new Slim();

//initialisation variable de session
session_start();

//-----------------------------routes----//

//----------------------Pages-d-accueil--------------//
$app->get('/', function () {
  $c = new GestionAccueil();
  $c -> afficheAccueil();
})->name("accueil");

$app->get('/contact', function () {
  $c = new GestionAccueil();
  $c -> afficheContact();
})->name("contact");

$app->get('/about', function () {
  $c = new GestionAccueil();
  $c -> afficheAbout();
})->name("about");

$app->get('/help', function () {
  $c = new GestionAccueil();
  $c -> afficheHelp();
})->name("help");

//-----------------------------------Gestion-du-compte-------------//
$app->get('/user', function () {
  $c = new GestionCompte();
  $c -> afficheUser();
})->name("aff-user");

$app->get('/user/remove', function () {
  $c = new GestionCompte();
  $c -> supprimerCompte();
})->name("supprimer-compte");

$app->get('/user/modif', function() {
  $c = new GestionCompte();
  $c -> afficheModifierInfoCompte();
} )-> name("modifier-infocompte");

$app->post('/user/modif', function() {
  $c = new GestionCompte();
  $c -> modifierInfoCompte($_POST);
} );

$app->get('/user/changermdp', function() {
  $c = new GestionCompte();
  $c -> afficheModifierMdp();
} )-> name("modifier-mdp");

$app->post('/user/changermdp', function() {
  $c = new GestionCompte();
  $c -> modifierMdp($_POST['old-pass'],$_POST['new-pass'],$_POST['conf-pass']);
} );

//----------------------Formulaire-Inscription-Compte------//
$app->get('/inscription', function () {
  $c = new GestionCompte();
  $c -> afficheInscription();
})->name("inscription");

$app->post('/inscription', function () {

  if( isset($_POST['valider-insc']) && $_POST['valider-insc'] == 'S\'inscrire'){

    $c = new GestionCompte();

    $valueFiltred = $c->filtrerInscription($_POST);

    if( !empty($valueFiltred) ){
      $c->ajouterUtilisateur($valueFiltred);
    }

  }
});

//-----------------------------Formulaire-de-connexion-et-deconnexion-compte----------//
$app->get('/connexion', function () {
  $c = new GestionCompte();
  $c -> afficheConnexion();
})->name("connexion");

$app->post('/connexion', function () {

  if( isset($_POST['connection']) && $_POST['connection'] == 'Connexion'){
    $c = new GestionCompte();
    $c->etablirConnection($_POST);
  }

});

$app->get('/deconnexion', function () {
  $c = new GestionCompte();
  $c -> deconnecter();
})->name("deconnexion");

//-------------------------------Gestion-Page-d'erreur------------------//
$app->get('/error/aucune_connexion', function () {
  $c = new GestionCompte();
  $c -> afficheNonConnexion();
})->name("no-connection");

$app->get('/error/forbidden', function () use ($app){
  $app->response()->status(403);
  $c = new GestionCompte();
  $c -> afficheNonAccess();
})->name("no-access");

$app->get('/error/list_not_found', function () use ($app){
  $app->response()->status(404);
  $c = new GestionListe();
  $c->afficherlisteNotFound();
})->name("list-not-found");

$app->notFound(function () use ($app) {
  $app->redirect($app->urlFor('list-not-found'));
});

//-----------------------------------Gestion-des-listes-utilisateurs--------//
$app->get('/list', function () {
  $c = new GestionListe();
  $c -> afficherAllListes();
})->name("liste");

$app->get('/list/creation', function () {
  $c = new GestionListe();
  $c -> afficherCreationListe();
})->name("liste-creation");

$app->post('/list/creation', function () {
  if( isset($_POST['valid-crea-list']) && $_POST['valid-crea-list'] == 'Créer'){
    $c = new GestionListe();
    $c -> creerListe($_POST);
  }
});

$app->get('/list/:idList', function ($idList) {
  $c = new GestionListe();
  $c->afficherListe($idList);
})->name("aff-liste");

$app->get('/list/:idList/remove', function ($idList) {
  $c = new GestionListe();
  $c->supprimerListe($idList);
})->name("del-liste");

$app->get('/list/:idList/modifier', function ($idList) {
  $c = new GestionListe();
  $c->afficherModifListe($idList);
})->name("modif-liste");

$app->post('/list/:idList/modifier', function ($idList) {
  $c = new GestionListe();
  $c->modifierListe($idList,$_POST);
});

$app->get('/list/:idList/token', function ($idList){
  $c = new GestionListe();
  $c->setToken($idList);
})->name("make-token");

//-------------------------------------------Gestion-des-items-----------//
$app->get('/list/:idList/addItem', function ($idList) {
  $c = new GestionItem();
  $c->afficherFormulaireItem($idList);
})->name("ajout-item");

$app->post('/list/:idList/addItem', function ($idList) {
  if( isset($_POST['valid-crea-item']) && $_POST['valid-crea-item'] == 'Ajouter'){
    $c = new GestionItem();
    $c->ajouterItem($_POST,$idList,$_FILES);
  }
});

$app->get('/list/:idList/delItem/:idItem', function ($idList,$idItem) {
    $c = new GestionItem();
    $c->supprimerItem($idList,$idItem);

})->name("remove-item");

//===============================================================================GESTION=PARTICIPANT====//
$app->get('/:token', function ($token) {
    $c = new GestionListe();
    $c->afficherListeParticipant($token);

})->name("aff-token");

$app->post('/:token/:idItem', function ($token,$idItem) {
    if( isset($_POST['valid-reserv']) && $_POST['valid-reserv'] == 'valid_reserv'){
      $c = new GestionListe();
      $c->reserverItem($token, $idItem, $_POST);
    }
})->name("reserv-item");

//Lancement de la route
$app->run();
